package com.associado.polijunior.tarefa_yuri.tarefa;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
   // private TextView txtOla;
    private Button botao;
    private EditText[] editores = new EditText[10];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        botao = (Button) findViewById(R.id.botaoEnviar);
        editores[0] = (EditText) findViewById(R.id.stringDadoNome);
        editores[1] = (EditText) findViewById(R.id.stringDadoIdade);
        editores[2] = (EditText) findViewById(R.id.stringDadoCpf);
        editores[3] = (EditText) findViewById(R.id.stringDadoDataNascimento);
        editores[4] = (EditText) findViewById(R.id.stringDadoEmail);
        editores[5] = (EditText) findViewById(R.id.stringDadoEstadoCivil);
        editores[6] = (EditText) findViewById(R.id.stringDadoEndereco);
        editores[7] = (EditText) findViewById(R.id.stringDadoNomePai);
        editores[8] = (EditText) findViewById(R.id.stringDadoNomeMae);
        editores[9] = (EditText) findViewById(R.id.stringDadoAlergia);

        botao.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String texto = "";
                for(int i = 0; i < 10; i++) {
                    if(editores[i].getText().toString().equals("")) {
                        texto += i + "null\n";
                        Log.i("EdtText", "null");
                    }
                    texto += editores[i].getText().toString() + "\n";
                    Log.i("EdtText", editores[i].getText().toString());
                    editores[i].setText("");
                }
                Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_LONG).show();
            }
        });
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
